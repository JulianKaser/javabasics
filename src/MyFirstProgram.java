import java.util.Scanner;

public class MyFirstProgram {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        //here I define a variable name of the datatype String
        //String name = "Julian";

        //int is a primitive datatype
        //int year = 1975;

        System.out.println("Bitte gib deinen Namen ein:");
        String name = scanner.nextLine();

        System.out.println("Hello, my name is "
                + "" + name);


        scanner.close();
    }
}
