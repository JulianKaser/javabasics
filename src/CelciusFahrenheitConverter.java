import java.util.Scanner;

public class CelciusFahrenheitConverter {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //hier definieren wir welche Variablen (mit Datentyp!) wir verwenden werden:
        int temperatur = 0;
        int resultat = 0;


        //User auffordern, eine Temperatur einzugeben

        //einlesen einer int zahl:
        temperatur = Integer.parseInt(scanner.nextLine());


        //User auffordern, was gerechnet wird - Celcius "C" oder Fahrenheit "F"
        //Eingabe Aufforderung:
        String input = scanner.nextLine();

        //hier berechnen wir das Resultat:
        if (input.equals("C")) {
            //berechnung mit Celcius
        } else if (input.equals("F")) {
            //berechnung mit Fahrenheit
        } else {
            System.out.println("Sorry, falsche Eingabe");
        }

        // hier geben wir das Resultat aus:

        scanner.close();

    }
}
